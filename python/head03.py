#! /usr/bin/python3
#-*- coding: utf-8-*-

# @aleixridameya ASIX M06 Curs 2023-2024
#--------------------------

# execució:  python3 head3.py < dades.txt (processa l'entrada estandard)
# ls -l ~ | python3 head3.py


import sys

MAX=2

fileIn=sys.stdin

if len(sys.argv)==2:
    fileIn=open(sys.argv[1],"r")

counter=0

for line in fileIn:
    counter += 1
    print(line,end="")
    if counter == MAX: break 

fileIn.close()

exit(0)