
#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# echo-server.py
#
# @aleixridameya Curs 2023-2024
# ----------------------------------------
#
# Execució: python3 21-echo-server.py 
# En un altre terminal: nc localhost 50001

import sys, socket

HOST = ''  # es nomes localhost ''
PORT = 50001

# Crea un objecte de tipus socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # STREAM: protocol TCP oritentat a connexió
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
s.bind((HOST,PORT)) #Escoltara per port 5001 i totes les interfícies del host
s.listen(1) #Li diem que es posi a escoltar
conn, addr = s.accept() # Ens dona una tupla pero la separem amb 2 variables (ÉS ON ES QUEDA ESCOLTANT)

# conn és un derivat de socket que representa una connexió ja establerta.
# s és una variable que representa un socket, un socket escolta per un pot.
print("Conn:", type(conn), conn)
print("Connected by:", addr)

while True:
    data = conn.recv(1024)
    if not data:break # Si ha tancat la connexió "break"
    conn.send(data)
    print(data)

conn.close()
sys.exit(0)