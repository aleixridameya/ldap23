#! /usr/bin/python3
#-*- coding: utf-8-*-

# programa -py ruta (positional argument)
#----------------------------
import sys, argparse
from subprocess import Popen, PIPE
#----------------------------------

parser = argparse.ArgumentParser(description=\
                                 """Consulta a a base de dades entrada per argument al programa.""")

parser.add_argument("sqlStatement",type=str,\
                    help="Consulta a la base de dades training")

args=parser.parse_args()

#---------------------------

# Execució de l'ordre who en el sistema
command = ["PGPASSWORD=passwd psql -qtA -F ',' -h localhost -U postgres training"]

#defineix una variable qualsevol
#Popen és un constructor, que construeix un pipe
#stdout=PIPE, la sortida del who va a parar al PIPE
pipeData = Popen(command, shell=True, bufsize=0, \
                 universal_newlines=True, \
                 stdout=PIPE, stdin=PIPE, stderr=PIPE)

pipeData.stdin.write(args.sqlStatement+"\n\q\n")


#La llegeix i la mostra per pantalla.
for line in pipeData.stdout: #Seria un .readline però no es posa.
    print(line, end="")

exit (0)