#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-fork.py
#
# @aleixridameya Curs 2023-2024
# ----------------------------------------
import sys,os

print("Hola, començament del programa principal")
print("PID pare:", os.getpid())

pid=os.fork() # Duplica el procés "genere un nou procés exacte amb diferent PID" 
              # El procés pare rep el PID del fill 
              # El procés fill rep el PID 0

if pid !=0:
    #os.wait() # Esperar que finilitzi el procés fill
    print("Programa pare: ", os.getpid(), pid)
else:
    print("Programa fill: ", os.getpid(), pid)
    while True:
       pass

print('Hasta luego lucas')
sys.exit(0)