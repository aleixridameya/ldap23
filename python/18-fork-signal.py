#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# fork-signal.py
#
# @aleixridameya Curs 2023-2024
# ----------------------------------------

import sys,os,signal

print("Hola, començament del programa principal")
print("PID pare:", os.getpid())

pid=os.fork()

if pid !=0:
    #os.wait()
    print("Programa pare: ", os.getpid(), pid)
    print("Llançant el procés fill servidor")
    sys.exit(0)
else:
    
    print("Programa fill: ", os.getpid(), pid)
    
    def myusr1(signum, frame):
        print("Hola radiola")
    
    def myusr2(signum, frame):
        print("Adeu andreu2")
        sys.exit(0)

    # Assignar un handler al senyal
    signal.signal(signal.SIGUSR1,myusr1)          #10
    signal.signal(signal.SIGUSR2,myusr2)          #12

    print(os.getpid())
    while True:
        pass




# No hauria de sortir mai.
print('Hasta luego lucas')
sys.exit(0)