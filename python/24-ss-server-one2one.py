#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# echo-server.py
#
# @aleixridameya Curs 2023-2024
# ----------------------------------------

import sys, socket, argparse, signal, os
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""Daytime server""")
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
llistaPeers = []
HOST = ''
PORT = args.port


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)



def myusr1(signum, frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers)
  sys.exit(0)


def myusr2(signum, frame):
  print("Signal handler called with signal:", signum)
  print(len(llistaPeers))
  sys.exit(0)


def myterm(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers, len(llistaPeers))
  sys.exit(0)

signal.signal(signal.SIGUSR1,myusr1) #10
signal.signal(signal.SIGUSR2,myusr2) #12
signal.signal(signal.SIGTERM,myterm) #15


while True:
  conn, addr = s.accept()
  print("Connected by", addr)
  command = ["ss","-ltp"]
  llistaPeers.append(addr)
  pipeData = Popen(command,stdout=PIPE)
  for line in pipeData.stdout:
    conn.send(line)
  conn.close()

