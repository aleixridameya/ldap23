#! /usr/bin/python3
#-*- coding: utf-8-*-

# popen_sql.py -c num_cli -c numcli...

#2111, 2102, 2103 Num clies.

# Execució exemple: python3 14-popen-sql-multi.py -c 2111 -c 2102 -c 2103


#----------------------------
import sys, argparse
from subprocess import Popen, PIPE
#----------------------------------

parser = argparse.ArgumentParser(description=\
                                 """Consulta a a base de dades entrada per argument al programa.""")

#parser.add_argument("sqlStatement",type=str,\
 #                   help="Consulta a la base de dades training")

parser.add_argument("-c","--con",type=str,\
                    dest="c", help="num_clie a procesar",\
                    metavar="c",\
                    action="append",\
                    required="True"
                    )

args=parser.parse_args()


#---------------------------

# Execució de l'ordre who en el sistema
command = ["PGPASSWORD=passwd psql -qtA -F ',' -h localhost -U postgres training"]

#defineix una variable qualsevol
#Popen és un constructor, que construeix un pipe
#stdout=PIPE, la sortida del who va a parar al PIPE
pipeData = Popen(command, shell=True, bufsize=0, \
                 universal_newlines=True, \
                 stdout=PIPE, stdin=PIPE, stderr=PIPE)

for c in args.c:
    sqlStatement="select * from clientes where num_clie=%s;" % (c)  #% (variable de la opció)
    pipeData.stdin.write(sqlStatement+"\n")
    print(pipeData.stdout.readline(), end="")

#for line in pipeData.stdout: #Seria un .readline però no es posa.
#    print(line, end="")
    



#La llegeix i la mostra per pantalla.


exit (0)