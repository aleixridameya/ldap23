#! /usr/bin/python3
#-*- coding: utf-8-*-

# @aleixridameya ASIX M06 Curs 2023-2024
#--------------------------
# execució: python3 head.py (El fitxer esta definit en el propi programa)


import sys

MAX=2

fileName="dades.txt"
fileIn=open(fileName,"r")

counter=0
for line in fileIn:
    counter += 1
    print(line,end="")
    if counter == MAX: break 

fileIn.close()

exit(0)