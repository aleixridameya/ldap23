#! /usr/bin/python3
#-*- coding: utf-8-*-

# programa -py ruta (positional argument)
#----------------------------
import sys, argparse
from subprocess import Popen, PIPE
#----------------------------------

# Execució de l'ordre who en el sistema
command = ["psql -qtA -F ',' -h localhost -U postgres training -c \"select * from clientes;\""]

#defineix una variable qualsevol
#Popen és un constructor, que construeix un pipe
#stdout=PIPE, la sortida del who va a parar al PIPE
pipeData = Popen(command, shell=True, stdout=PIPE)
#La llegeix i la mostra per pantalla.
for line in pipeData.stdout:
    print(line.decode("utf8"), end="")

exit (0)