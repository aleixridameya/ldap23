
#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# echo-client.py
#
# @aleixridameya Curs 2023-2024
# ----------------------------------------


import sys, socket

HOST = ''  # es nomes localhost ''
PORT = 50001

# Crea un objecte de tipus socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # STREAM: protocol TCP oritentat a connexió
#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

s.connect((HOST,PORT)) # Fins que no fa connect no es realitza la connexió
s.send(b'Hello,word') # Li envia un text "al servidor"
data = s.recv(1024) 

s.close()
print("Received", repr(data))
sys.exit(0)