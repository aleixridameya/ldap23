#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# telnet-server.py
#
# @aleixridameya Curs 2023-2024
# ----------------------------------------

import sys, socket, argparse, signal, os, time
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""Daytime server""")
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
llistaPeers = []
HOST = ''
PORT = args.port
MYEOF = bytes(chr(4), 'utf-8')

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)


def myusr1(signum, frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers)
  sys.exit(0)


def myusr2(signum, frame):
  print("Signal handler called with signal:", signum)
  print(len(llistaPeers))
  sys.exit(0)


def myterm(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers, len(llistaPeers))
  sys.exit(0)

    
signal.signal(signal.SIGUSR1,myusr1) #10
signal.signal(signal.SIGUSR2,myusr2) #12
signal.signal(signal.SIGTERM,myterm) #15

pid=os.fork()
if pid !=0:
  print("Server PS:", pid)
  sys.exit(0) 

while True:
  conn, addr = s.accept()
  print("Connected by", addr)
  llistaPeers.append(addr)
  while True:
    data = conn.recv(1024)
    if not data : break
    pipeData = Popen(data,stdout=PIPE,stderr=PIPE,shell=True)
    for line in pipeData.stdout:
        conn.sendall(line)
    for line in pipeData.stderr:
        conn.sendall(line)
    conn.send(MYEOF)
  conn.close()

