#! /usr/bin/python3
#-*- coding: utf-8-*-

# @aleixridameya ASIX M06 Curs 2023-2024
#--------------------------

import argparse

parser = argparse.ArgumentParser(\
    description="programa d'exemple d'arguments",\
    prog="02-arguments.py",\
    epilog="hata luego lucas!")

parser.add_argument("-n","--nom", type=str,\
    help = "nom ususari")

parser.add_argument("-e","--edat",type=int,\
                    dest="userEdat", help="edat a processar",\
                    metavar="edat")

args = parser.parse_args()


print(args)
print(args.userEdat,args.nom)
exit(0)