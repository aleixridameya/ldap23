#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# signal-exemple.py
#
# @aleixridameya Curs 2023-2024
#
# Una vegada hem executat aquest programa i el "matem" kill -10 (num_proces) i ens mostrarà els print de la funció
#  kill -10 $(pgrep python)
#--------------------------

import sys, os, signal

def myhandler(signum, frame):
    print("Signal handler with signal: ", signum)
    print("Hasta luego Lucas!")
    sys.exit(0)

def nodeath(signum, frame):
    print("Signal handler with signal: ", signum)
    print("No hem dona la gana de plegar")

# Assignar un handler al senyal

signal.signal(signal.SIGUSR1,myhandler) # senyal 10
signal.signal(signal.SIGALRM,myhandler) # senyal 14
signal.signal(signal.SIGUSR2,nodeath) # senyal 12

signal.signal(signal.SIGTERM,signal.SIG_IGN) # senyal 15
signal.signal(signal.SIGINT,signal.SIG_IGN) # senyal 2


signal.alarm(60)

print(os.getpid())

while True:
    pass  # No fa res "pass"

sys.exit(0)

