#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-execv.py
#
# @aleixridameya Curs 2023-2024
# ----------------------------------------

import sys,os,signal

print("Hola, començament del programa principal")
print("PID pare:", os.getpid())

pid=os.fork()

if pid !=0:
    #os.wait()
    print("Programa pare: ", os.getpid(), pid)
    print("Llançant el procés fill servidor")
    sys.exit(0)

# Programa fill

# Enlloc de ser el procés actual es transforma en un altre proces, a dins dels []
# Tot el que hi ha abans de execv, es perd, ja que es substitueix per que ho hagi
# al execv  

#os.execv("/usr/bin/ls", ["/usr/bin/ls", "-la", "/"]) 
#os.execl("/usr/bin/ls","/usr/bin/ls","-la","/")

#os.execlp("ls","ls","-la","/")
#os.execvp("uname",["uname","-a"])

#os.execv("/bin/bash",["/bin/bash", "show.sh"])

os.execlpe("/bin/bash","/bin/bash", "show.sh", {"edat":"40", "nom":"jorge"})

print('Hasta luego lucas')
sys.exit(0)