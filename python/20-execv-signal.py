
#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-execv.py
#
# @aleixridameya Curs 2023-2024
# ----------------------------------------

import sys,os,signal

print("Hola, començament del programa principal")
print("PID pare:", os.getpid())

pid=os.fork()

if pid !=0:
    #os.wait()
    print("Programa pare: ", os.getpid(), pid)
    print("Llançant el procés fill servidor")
    sys.exit(0)

# Programa fill
os.execv("/usr/bin/python3", ["/usr/bin/python3", "16-signal.py","-t","6"])

print("Hasta luego")
sys.exit(0)
