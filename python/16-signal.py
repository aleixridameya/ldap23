#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# signal-exemple.py
#
# @aleixridameya Curs 2023-2024
#
# Una alarma que: 
# rep senyal SIGUSR1 +60
# rep senyal SIGUSR2 -60
# rep senyal HUB reinicia
# rep senyal SIGTERM temps restant de l'alarma
# Cuando acaba la alarma mostra:
# uppers: quantes vegades ha pujat el marcador
# down:  quantes vegades ha baixat el marxador
# temps que queda
# No ctrl + C
#--------------------------

import sys, os, signal, argparse


parser = argparse.ArgumentParser(\
    description="programa que mostra lineas d'un fitxer",\
    prog="16-signal.py")

parser.add_argument("-t","--time",type=int,\
                    help="temps a processar",\
                    metavar="temps", \
                    dest="temps")

args = parser.parse_args()

temps = args.temps

up=0
down=0

def myusr1(signum, frame):
    actual = signal.alarm(0)
    signal.alarm(actual+60)
    global up 
    up +=1
    print("Signal handler with signal: ", signum)

def myusr2(signum, frame):

    
    actual = signal.alarm(0)
    global down
    down +=1
    if actual > 60:
        signal.alarm(actual-60)
    else: 
        print("No pots restar menys temps del que queda, temps actual:", actual)
        signal.alarm(actual)
    
    print("Signal handler with signal:", signum)


def myalarm(signum,frame):
  print("Signal handler called with signal:", signum)
  print("Finalitzant... up: %d down:%d restant: %d" % (up, down, signal.alarm(0)))
  sys.exit(0)

def myterm(signum,frame):
  actual = signal.alarm(0)
  signal.alarm(actual)
  print("Signal handler called with signal:", signum)
  print("Temps restant: ", actual)


def myhup(signum,frame):
  signal.alarm(temps)
  print("Signal handler called with signal:", signum)
  print("Alarma reiniciada, temps restant: ", temps)


# Assignar un handler al senyal
signal.signal(signal.SIGUSR1,myusr1)          #10
signal.signal(signal.SIGUSR2,myusr2)          #12
signal.signal(signal.SIGALRM,myalarm)         #14
signal.signal(signal.SIGTERM,myterm)          #15
signal.signal(signal.SIGHUP,myhup)            #1
signal.signal(signal.SIGINT,signal.SIG_IGN)   #2


signal.alarm(temps)

print(os.getpid())

while True:
    pass  # No fa res "pass"

sys.exit(0)

