#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# telnet-client.py
#
# @aleixridameya Curs 2023-2024
# ----------------------------------------
import sys,socket,argparse 


parser = argparse.ArgumentParser(description="""Client """)
parser.add_argument("server",type=str)
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()

HOST = args.server
PORT = args.port
MYEOF = bytes(chr(4), 'utf-8')

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

command = []

while True:
  command = input("~$ ")
  if not command: break
  s.sendall(bytes(command, 'utf-8'))
  while True:
    data = s.recv(1024)
    if data[-1:] == MYEOF :
      print(str(data[:-1]))
      break # Si la ultima dada que he rebut....és la dessitjada break
    print(data)

s.close()
sys.exit()