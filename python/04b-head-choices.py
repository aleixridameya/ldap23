#! /usr/bin/python3
#-*- coding: utf-8-*-

# @aleixridameya ASIX M06 Curs 2023-2024
#
# head [-n 5|10|15 nlin] [-f file]
# default=10 file o stdin
#--------------------------

# execució:  python3 head3.py < dades.txt (processa l'entrada estandard)
# ls -l ~ | python3 head3.py

import argparse,sys


parser = argparse.ArgumentParser(\
    description="programa que mostra lineas d'un fitxer",\
    prog="head04.py")


parser.add_argument("-n", type=int,\
                    dest="nlin", help="linea a mostrar",\
                    metavar="nlin",\
                    choices=[5, 10, 15],\
                    default=10)

parser.add_argument("fitxer",type=str,\
                    help="fitxer a processar",\
                    metavar="fitxer")

args = parser.parse_args()

MAX = args.nlin

counter=0
fileIn = open(args.fitxer,"r")

for line in fileIn:
    counter += 1
    print(line,end="")
    if counter == MAX: break 

fileIn.close()

exit(0)