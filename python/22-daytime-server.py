
#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# echo-server.py
#
# @aleixridameya Curs 2023-2024
# ----------------------------------------
#
# Execució: python3 22-daytime-server.py 
# En un altre terminal: nc localhost 50001 "Ens ha de mostrar la data actual"

import sys, socket
from subprocess import Popen, PIPE

HOST = ''  # es nomes localhost ''
PORT = 50001

# Crea un objecte de tipus socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # STREAM: protocol TCP oritentat a connexió
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
s.bind((HOST,PORT)) #Escoltara per port 5001 i totes les interfícies del host
s.listen(1) #Li diem que es posi a escoltar
conn, addr = s.accept() # Ens dona una tupla pero la separem amb 2 variables (ÉS ON ES QUEDA ESCOLTANT)

command = ["date"]
# command = ["lsof"]

pipeData = Popen(command, stdout=PIPE)

print("Conn:", type(conn), conn)
print("Connected by:", addr)

for line in pipeData.stdout: #Seria un .readline però no es posa.
    print(line)
    conn.send(line)

conn.close()
sys.exit(0)