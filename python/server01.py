#! /usr/bin/python3
# -*- coding 'utf8' -*-
# Aleix Ridameya
# server01.py [-d,--debug] [-p,--port]
# REQUERIMENTS:
# args, funcions signal, assignacions signal
# fork -> plegar pare
# socket
# -----------------------------------



import sys, os, socket, argparse, signal
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(\
        description="Programa",\
        prog="server01.py")

parser.add_argument("-d","--debug",\
        action="store_true")

parser.add_argument("-p","--port",\
        type=int,\
        dest="port",help="port per on vols que es connectin al servidor.",\
        metavar="port",\
        default="44444")

args=parser.parse_args()

HOST=""
PORT=args.port
llistaPeers=[]
MYEOF=bytes(chr(4),'utf8')

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
s.bind((HOST,PORT))
s.listen(1)


def connections(signum,frame):
    print(HOST,PORT)
    sys.exit(0)

def total_rebut(signum,frame):
    print(len(llistaPeers))
    sys.exit(0)

def rebut(signum,frame):
    print('Vegades connectats: ',len(llistaPeers))
    print('LLista de connectats: ',llistaPeers)
    sys.exit(0)

signal.signal(signal.SIGUSR1,connections) #10
signal.signal(signal.SIGUSR2,total_rebut) #12
signal.signal(signal.SIGTERM,rebut) #15


while True:
    conn,addr=s.accept()
    llistaPeers.append(addr)
    while True:
        command = conn.recv(1024)
        if not command: break
        elif command.strip() == b"processos":
            command = ['ps ax']
        elif command.strip() == b"ports": 
            command = ['ss -pla']
        #elif command.strip() == b"":
         #   sys.exit(0)
        else:
            command = ['uname -a']
        pipeData=Popen(command,stdout=PIPE,stdin=PIPE,shell=True) 
        for line in pipeData.stdout:
            conn.sendall(line)
        conn.sendall(MYEOF)
    conn.close()

