#! /usr/bin/python3
#-*- coding: utf-8-*-

# @aleixridameya ASIX M06 Curs 2023-2024
#
# head [-n 5|10|15 nlin] [-f file] ...
# default=10 file o stdin
#--------------------------

# execució:  python3 05-head-multi.py -n 5 -f 02-exemple-args.py  -f 04-head-choices.py  (processa l'entrada estandard)


import argparse,sys


parser = argparse.ArgumentParser(\
    description="programa que mostra lineas d'un fitxer",\
    prog="head04.py")


parser.add_argument("-n", type=int,\
                    dest="nlin", help="linea a mostrar",\
                    metavar="nlin",\
                    choices=[5, 10, 15],\
                    default=10)

parser.add_argument("-f",type=str,\
                    dest="fileList", help="fitxer a processar",\
                    metavar="fitxer",\
                    action="append"
                    )

args = parser.parse_args()

MAX = args.nlin


def headFile(fitxer):

    counter=0
    fileIn = open(fitxer,"r")

    for line in fileIn:
        counter += 1
        print(line,end="")
        if counter == MAX: break 
    fileIn.close()
    
for fileName in args.fileList:
    headFile(fileName)


exit(0)