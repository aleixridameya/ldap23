#! /usr/bin/python3
#-*- coding: utf-8-*-

# @aleixridameya ASIX M06 Curs 2023-2024
#
# head [-n 5|10|15 ] [-f file] ...
# default=10 file
#--------------------------

# execució:   python3 05b-head-multi.py 02-exemple-args.py 04-head-choices.py -n 5  (processa l'entrada estandard)


import argparse,sys


parser = argparse.ArgumentParser(\
    description="programa que mostra lineas d'un fitxer",\
    prog="head04.py")


parser.add_argument("-n", type=int,\
                    dest="nlin", help="linea a mostrar",\
                    metavar="nlin",\
                    choices=[5, 10, 15],\
                    default=10)

parser.add_argument("fileList",type=str,\
                    help="fitxer a processar",\
                    metavar="fitxer",\
                    nargs="+")


parser.add_argument("-v","--verbose",\
                    action="store_true")

args = parser.parse_args()

MAX = args.nlin


def headFile(fitxer):
    counter=0
    fileIn = open(fitxer,"r")

    for line in fileIn:
        counter += 1
        print(line,end="")
        if counter == MAX: break
        
    fileIn.close()

if args.fileList:
    for fileName in args.fileList:
        if args.verbose:
            print("\n",fileName, 40*"-")
        
        headFile(fileName)


exit(0)