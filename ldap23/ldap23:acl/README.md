## ordres
```
ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' 
```

## Llistar els atributs del OlcAccess

```
ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
```

## Modificar olcAccess

```
Necessitem utilitar un fitxer .ldif


dn: olcDatabase={1}mdb,cn=config
changetype: modify
delete: olcAccess
-
add: olcAccess
olcAccess: to * by * read

ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f acl1.ldif 

Per comprovar els canvis tornem a fer: ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess

```

## Fitxer de configuració client (modificar per no haver d'especificar la bd)

```
vim /etc/ldap/ldap.conf

Afegir lineas:

BASE dc=edt,dc=org
URI ldap://ldap.edt.org

Fer docker cp per tenir-ho en el directori

docker cp ldap:/etc/ldap/ldap.conf .

Afegir linea en startup.sh utilitzant el fitxer ldap.conf "personalitzat"
```

## Consultes
```
Anonim
ldapsearch -x -LLL dn mail

User
ldapsearch -x -LLL -D 'cn=Anna Pou,ou=usuaris,dc=edt,dc=org' -w anna dn mail

Els usuaris no poden canviar les dades dels altres usuaris.
ldapmodify -vx -D 'cn=Anna Pou,ou=usuaris,dc=edt,dc=org' -w anna -f mod1.ldif 
ldap_initialize( <DEFAULT> )
replace mail:
	newmail@edt
modifying entry "cn=Pau Pou,ou=usuaris,dc=edt,dc=org"
ldap_modify: Insufficient access (50)

```


