### A partir del ldap23:base utilitzarem els fitxers (Dockerfile, startup.sh, edt-org.ldif, sldapd.conf) i a partir els editarem.

## Generar fitxer edt-org.ldif (usuaris 0-11) i modificant dn dels usuaris utilitzant en lloc del cn el uid per identificar-los (rdn).
```
Editarem al fitxer afegint nous usuaris i canviant el cn per uid.
Les passwords estaran xifrades i per poder-las xifra utilitzarem:
  - slappasswd -h {SSHA} -s user0

Els usuaris creats seran del 0 al 11 amb aquest format:

dn: uid=user0,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: usuari especial 0
cn: usuari 0
sn: especial 0
homephone: 111-222-333
mail: user0@gmail.com
description: soc un usuari
ou: user_especial
uid: user0
uidNumber: 11
gidNumber: 600
homeDirectory: /tmp/home/user0
userPassword: {SSHA}6vq3BoWeCvaaa40F7bI6qjirLUWP81xR

```

## Afegir en el fitxer dos usuaris i una ou nova inventada i posar-los dins la nova ou.

```
Afegirem 1 ou nova i 2 usuaris que formaran part del nou ou:

OU:

dn: ou=manteniment,dc=edt,dc=org
ou: manteniment
description: Caps de manteniment 
objectclass: organizationalunit

USER:

dn: cn=electric,ou=manteniment,dc=edt,dc=org
objectclass: inetOrgPerson
cn: electric
sn: electron
mail: electric@gmail.com
description: manteniment electric

```

## Configurar el password de Manager que sigui ‘secret’ però  encriptat (posar-hi un comentari per indicar quin és de cara a estudiar).

```
Per encriptar secret utilitzarem la ordre slappasswd:

slappasswd -h {SSHA} -s secret

Editar fitxer de configuració slapd.conf: 

#contra = secret
rootpw {SSHA}Sh8b/TLyQVPc1AaKU2Gu1+S5ntWiKSjC
```

## Afegir usuari (admin) per poder consultar i modificar les dades ldap en calent en qualsevol bd de ldap.
```
database config
rootdn "cn=sysadmin,cn=config"
rootpw syskey
``` 

## Crear imatge ldap23:editat
```
docker build -t aleixridameya/ldap_editat:base .
```
## Inicialitzar container ldap 

```
docker run --rm -h ldap.edt.org --name ldap -p 389:389 -d aleixridameya/ldap_editat:base
```

## Entrar al container i fer proves en calent

```
docker exec -it ldap /bin/bash

ldapsearch -x -LLL -D "cn=sysadmin,cn=config" -w syskey -b "dc=edt,dc=org" | grep dn

dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: ou=manteniment,dc=edt,dc=org
dn: cn=electric,ou=manteniment,dc=edt,dc=org
dn: cn=informatic,ou=manteniment,dc=edt,dc=org
dn: cn=pau,ou=usuaris,dc=edt,dc=org
dn: cn=pere,ou=usuaris,dc=edt,dc=org
dn: uid=anna,ou=usuaris,dc=edt,dc=org
dn: uid=marta,ou=usuaris,dc=edt,dc=org
dn: uid=jordi,ou=usuaris,dc=edt,dc=org
dn: uid=admin System,ou=usuaris,dc=edt,dc=org
dn: uid=user0,ou=usuaris,dc=edt,dc=org
dn: uid=user1,ou=usuaris,dc=edt,dc=org
dn: uid=user2,ou=usuaris,dc=edt,dc=org
dn: uid=user3,ou=usuaris,dc=edt,dc=org
dn: uid=user4,ou=usuaris,dc=edt,dc=org
dn: uid=user5,ou=usuaris,dc=edt,dc=org
dn: uid=user6,ou=usuaris,dc=edt,dc=org
dn: uid=user7,ou=usuaris,dc=edt,dc=org
dn: uid=user8,ou=usuaris,dc=edt,dc=org
dn: uid=user9,ou=usuaris,dc=edt,dc=org
dn: uid=user10,ou=usuaris,dc=edt,dc=org
dn: uid=user11,ou=usuaris,dc=edt,dc=org
 
```
```
Per poder fer modificacions a la bd amb sysadmin hem de canviar-li els permisos a la bd 1 (edt.org):

ldapmodify -x -D 'cn=sysadmin,cn=config' -w syskey -f permisos.ldif   
modifying entry "olcDatabase={1}mdb,cn=config"

permisos.ldif:
dn: olcDatabase={1}mdb,cn=config
changetype: modify
delete: olcAccess
-
add: olcAccess
olcAccess: to * by * write
```
```
Per modificar la passwd:

ldappasswd -x -D 'cn=sysadmin,cn=config' -w syskey -S 'uid=user0,ou=usuaris,dc=edt,dc=org'
New password: 
Re-enter new password: 

Comprovar que ha canviat la passwd del usuari escollit:

ldapwhoami -x -D 'uid=user0,ou=usuaris,dc=edt,dc=org' -w hola
dn:uid=user0,ou=usuaris,dc=edt,dc=org

També es pot fer amb fitxer:
ldapmodify -x -D 'cn=sysadmin,cn=config' -w syskey -f novapasswd.ldif

novapasswd.ldif:
dn: uid=user0,ou=usuaris,dc=edt,dc=org
changetype: modify
replace: userPassword
userPassword: user00
```
```
Canvi de nom 

ldapmodify -x -D 'cn=sysadmin,cn=config' -w syskey -f newname.ldif 

newname.ldif:
changetype: modify
replace: cn
cn: josep

Comprovar que ha canviat el nom:

ldapsearch -x -LLL -D "cn=Sysadmin,cn=config" -w syskey -b 'dc=edt,dc=org' 'uid=user0'
```

