## Creació d'un servidor ldap en mode detach.

### 1 Crear directori context
```
mkdir ldap_deures:base
cd ldap_deures:base
```

### 2 Crear el Dockerfile
```
FROM debian:latest
LABEL author="aleix"
LABEL subjecte="ldapserver 2023"
RUN apt-get update
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd
ARG DEBIAN_FRONTED=noninteractive
RUN mkdir /opt/docker
COPY * /opt/docker
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```

### 3 Crear el startup.sh
```
#! /bin/bash


# Server bàsic ldap, amb base de dades edt.org. Aquesta imatge engega amb CMD un script anomenat startup.sh que fa el següent:
# 1 Esborrar els directoris de configuració i de dades
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
# 2  Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.confi
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
# 3  Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
# 4  Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
# 5  Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground
/usr/sbin/slapd -d0

```

### 4 Crear fitxer de configuracio slapd.conf
```
Copiem un fitxer predefinit
```


### 5 Crear ditxer de les dades de la base de dades edt-org.ldif
```
Copiem un fitxer predefinit
```

### 6 Crea imatge
```
docker build -t aleixridameya/ldap_deures:base
```

### 7 Crear el container
```
docker run --rm -h ldap.edt.org -d aleixridameya/ldap_deures:base
```
### 8 Fer consultes per comprobar que funcioni
```
ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org'
ldapsearch -x -LLL -b ‘ou=usuaris,dc=edt,dc=org’ 
```
