## 03-ldap-practica-schema

### Schema amb un objecte STRUCTURAL, AUXILIAR (min 3 atributs en cada objecte)

#### processadors.schema
```
# processadors.schema
#
# x-model
# x-nom
# x-nuclis
# x-fils
# x-PCIe4-0
# x-package
# x-imatge
# x-inf
#
# Derivat de top, structural i auxiliar
# -----------------------------------------

attributetype (1.1.2.1.1 NAME 'x-model'
 DESC 'Model del processador'
 EQUALITY caseIgnoreMatch 
 SUBSTR caseIgnoreSubstringsMatch
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
 SINGLE-VALUE )

attributetype (1.1.2.1.2 NAME 'x-nom'
 DESC 'Nom complet del processador'
 EQUALITY caseIgnoreMatch 
 SUBSTR caseIgnoreSubstringsMatch
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
 SINGLE-VALUE )

attributetype (1.1.2.1.3 NAME 'x-package'
 DESC 'Socket compatible'
 EQUALITY caseIgnoreMatch 
 SUBSTR caseIgnoreSubstringsMatch
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
 SINGLE-VALUE )

attributetype (1.1.2.1.4 NAME 'x-PCIe4-0'
 DESC 'compatibilitat amb PCIe 4.0 true/false'
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
 SINGLE-VALUE )

attributetype (1.1.2.1.5 NAME 'x-nuclis'
 DESC 'nuclis del processador'
 EQUALITY integerMatch
 ORDERING integerOrderingMatch
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
 SINGLE-VALUE )

attributetype (1.1.2.3.1 NAME 'x-fils'
 DESC 'fils del processador'
 EQUALITY integerMatch
 ORDERING integerOrderingMatch
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
 SINGLE-VALUE )

attributetype (1.1.2.3.2 NAME 'x-imatge'
 DESC 'Foto del processador'
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.28 )

attributetype (1.1.2.3.3 NAME 'x-inf'
 DESC 'pdf amb inf extra'
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.5 )

objectClass (1.1.2.2.1 NAME 'x-procesador'
 DESC 'Processador inf'
 SUP TOP
 STRUCTURAL
 MUST ( x-model $ x-nom $ x-package )
 MAY ( x-nuclis $ x-PCIe4-0 )
 )

objectClass (1.1.2.2.2 NAME 'x-procesador-extra'
 DESC 'Processador inf extra'
 SUP TOP
 AUXILIARY
 MUST x-inf
 MAY ( x-fils $ x-imatge )
 )
```

### Afegir ou=practica amb 3 noves identitats a dintre

#### edt-org.ldif
```
dn: ou=practica,dc=edt,dc=org
ou: practica
description: processadors
objectclass: organizationalunit

dn: x-model=5600X,ou=practica,dc=edt,dc=org
objectclass: x-procesador
objectclass: x-procesador-extra
x-model: 5600X
x-nom: AMD Ryzen 5 5600X 3.7GHz
x-package: AM4
x-nuclis: 8
x-fils: 16
x-PCIe4-0: True
x-imatge:< file:/opt/docker/55600x.jpg
x-inf:< file:/opt/docker/55600x.pdf

dn: x-model=7600X,ou=practica,dc=edt,dc=org
objectclass: x-procesador
objectclass: x-procesador-extra
x-model: 7600X
x-nom: AMD Ryzen 5 7600X 4.7 GHz
x-package: AM5
x-nuclis: 6
x-fils: 12
x-PCIe4-0: False
x-imatge:< file:/opt/docker/57600x.jpg
x-inf:< file:/opt/docker/57600x.pdf

dn: x-model=5800X,ou=practica,dc=edt,dc=org
objectclass: x-procesador
objectclass: x-procesador-extra
x-model: 5800X
x-nom: AMD Ryzen 7 5800X 3.8GHz
x-package: AM4
x-nuclis: 8
x-fils: 16
x-PCIe4-0: True
x-imatge:< file:/opt/docker/75800x.jpg
x-inf:< file:/opt/docker/75800x.pdf
```

### Edició del slapd.conf per nomes tenir els schemas necessaris

#### slapd.conf (important afegir el schema personalitzat)
```
include		/etc/ldap/schema/core.schema             # Nucli
include		/etc/ldap/schema/cosine.schema           # Dependències de inetorgperson
include		/etc/ldap/schema/inetorgperson.schema    # Atributs de persones
include		/etc/ldap/schema/nis.schema              # Conte el posixAccount
include		/opt/docker/processadors.schema          # El nostre schema
```

### Desplagament del container
```
docker build -t aleixridameya/ldap23:practica .
docker run --rm -h ldap.edt.org --network 2hisx -p 389:389 --name ldap.edt.org -d aleixridameya/ldap23:practica
docker run --rm -h phpldapadmin.edt.org --network 2hisx --name phpldapadmin.edt.org -p 80:80 -d aleixridameya/phpldapadmin:base
```

### Desplagament del container amb compose
```
docker compose up -d
[+] Running 3/3
 ✔ Network ldap23practica_mynet  Created                                                                                               0.1s 
 ✔ Container ldapserver          Started                                                                                               0.0s 
 ✔ Container phpldap             Started  
```

### Comporobació
```
En el navegador:

http://localhost/phpldapadmin

Accedir i mirar

En la terminal:

docker exec it ldap.edt.org /bin/bash

slapcat o ldapsearch -x -LLL -D 'cn=Manager,dc=edt,dc=org' -w secret -b 'ou=practica,dc=edt,dc=org' x-nom

```
