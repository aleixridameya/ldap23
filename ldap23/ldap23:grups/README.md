### Ldap server, ldap23:groups / ldap23:latest
- Nou ou en el fitxer edt-org.ldif per afegir grups
- definir grups posixGroup
- verificar el llistat d'usuaris i grups i la coherència de les dades.
- ldap ha d'escoltar tots els serveis


### 1.- Modifació de RDN per a que els usuaris s'identifiquin amb uid.
```
dn: uid=pere,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pere Pou
sn: Pou
homephone: 555-222-2221
mail: pere@edt.org
description: Watch out for this guy
ou: Profes
uid: pere
uidNumber: 5001
gidNumber: 100
homeDirectory: /tmp/home/pere
userPassword: {SSHA}ghmtRL11YtXoUhIP7z6f7nb8RCNadFe+
```

### 2.- Afegir entitat oranizationalUnit anomenada grups (editar fitxer edt-org.ldif)
```
dn: ou=groups,dc=edt,dc=org
ou: groups
description: Container per a grups
objectclass: organizationalunit
```

### 3.- Crear cadascun dels grups amb entitats de tipus posixAccount
#### Grups:
- professors (601)
- alumnes (600)
- 1asix (610)
- 2asix (611)
- sudo (27)
- 1wiam (612)
- 2wiam (613)
- 1hiaw (614)

Exemple de grup:
```
dn: cn=professors,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: professors
gidNumber: 601
description: Grup de professors
memberUid: pau
memberUid: pere
memberUid: jordi
```

### 4.- Modificar user admin perquè sigui del grup 27 sudo
```
dn: uid=admin,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Administrador Sistema
cn: System Admin
sn: System
homephone: 555-222-2225
mail: anna@edt.org
description: Watch out for this girl
ou: system
ou: admin
uid: admin
uidNumber: 10
gidNumber: 27
homeDirectory: /tmp/home/admin
userPassword: {SSHA}4mS0FycWc5bkpW8/a396SGNDTQUlFSX3ç
```

### 5.- Eliminar la conf de slapd.conf "includes"
Necessaris:
```
include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/inetorgperson.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openldap.schema
```

### 6.- Modifiació del startup.sh
- Protocol ldap: 389
- Protocol ldaps i ldapi: 636
```
#! /bin/bash
 
#   Esborrar els directoris de configuració i de dades
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
#   Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.confi
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d 
#   Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
#   Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
#   Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground
/usr/sbin/slapd -d0 -h 'ldap:/// ldaps:/// ldapi:///'
```

### 7.- Desplagament container
```
docker build -t aleixridameya/ldap23:grups .

docker run --rm -h ldap.edt.org --network 2hisx -p 389:389 -p 636:636 --name ldap.edt.org -d aleixridameya/ldap23:grups

docker exec -it ldap.edt.org /bin/bash
```

### 8.- Comprovació
```
Comprovar grups:
ldapsearch -xv -LLL -D 'cn=Manager,dc=edt,dc=org' -w secret -b 'ou=grups,dc=edt,dc=org'

alumnes: ldapsearch -xv -LLL -D 'cn=Manager,dc=edt,dc=org' -w secret -b 'dc=edt,dc=org' gidNumber=600
professors: ldapsearch -xv -LLL -D 'cn=Manager,dc=edt,dc=org' -w secret -b 'dc=edt,dc=org' gidNumber=601
...
```
