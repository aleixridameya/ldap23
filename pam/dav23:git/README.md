#Pràctica PAM 09.03 dav23:git

### Edició del Dockerfile
Afegir paquets necessaris: apache2 davfs2
```
FROM debian:latest
LABEL version="1.0"
LABEL subject="PAM host"
RUN apt-get update
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim less git finger passwd apache2 davfs2
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```

### Fitxers necessaris
**apache2-webdav.conf**
```
Alias /webdav /var/www/webdav
<Location /webdav>
  DAV On
  Require all granted
</Location>
```


### Edició startup.sh

```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

mkdir /var/www/webdav
cd /var/www/webdav
chgrp www-data /var/www/webdav
chmod g+w /var/www/webdav
git clone https://gitlab.com/edtasixm06/m06-aso.git
cd /opt/docker

a2enmod dav_fs
a2enmod auth_digest

cp apache2-webdav.conf /etc/apache2/conf-enabled

apachectl -DFOREGROUND -k start

```

### Inicialització
```
docker build -t aleixridameya/div23:git . 
```

### Docker-compose
```
version: "2"
services:
  ldap:
    image: aleixridameya/ldap23:latest
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    networks:
      - 2hisx
  pam:
    image: aleixridameya/pam23:ldap
    container_name: pam.edt.org
    hostname: pam.edt.org
    privileged: true
    networks:
      - 2hisx
  dav:
    image: aleixridameya/dav23:git
    container_name: dav.edt.org
    hostname: dav.edt.org
    privileged: true
    ports:
      - "80:80"
    volumes:
      - "data-dav:/var/www/webdav"
    networks:
      - 2hisx
networks:
  2hisx:
volumes:
  data-dav: 
```
