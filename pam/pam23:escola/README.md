#Pràctica PAM 09.02 Autenticació LDAP

## Requeriments
Crearem el pam "pam23:escola" a partir d'una copia de "pam23:ldap"


### Edició del Dockerfile
Afegir paquets necessaris: nslcd, nslcd-utils, ldap-utils, libnss-ldapd, libpam-ldapd
Afegir fitxers necessaris: ldap.conf, nslcd.conf, nsswitch.conf, common-session, pam_mount.conf.xml
```
FROM debian:latest
LABEL version="1.0"
LABEL subject="PAM host"
RUN apt-get update
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount nslcd nslcd-utils ldap-utils libnss-ldapd libpam-ldapd
RUN mkdir /opt/docker
COPY * /opt/docker/
COPY ldap.conf /etc/ldap/
COPY nslcd.conf /etc/
COPY nsswitch.conf /etc/
COPY common-session /etc/pam.d/
COPY pam_mount.conf.xml /etc/security/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```

### Fitxers necessaris
**/etc/ldap/ldap.conf**: Fitxer que ens permetra realitzar la connexió sense utilitzar IP i base de dades amb el servidor ldap.
**Comprovació: **
```
BASE	dc=escoladeltreball,dc=org
URI	ldap://ldap.escoladeltreball.org
```

**/etc/nslcd.conf**: Anomena la BD on estan els users i grups.
```
# The location at which the LDAP server(s) should be reachable.
uri ldap://ldap.escoladeltreball.org 

# The search base that will be used for all queries.
base dc=escoladeltreball,dc=org
```
**/etc/nsswitch.conf**: Posem l'opció que els usuaris i grups també estan en ldap segint un ordre de prioritat.
**Comprovació: getent passwd "usuaris del ldap" **
```
passwd:         files ldap
group:          files ldap
```
**/etc/pam.d/common-session**: Fitxer on afegim l'opció que al iniciar sessió amb un usuari es creei el home.
Linea de pam_mkhomedir.so
```
# and here are more per-package modules (the "Additional" block)
session	required	pam_unix.so 
session optional	pam_mkhomedir.so
session	optional	pam_mount.so 
session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
# end of pam-auth-update config
```

**/etc/security/pam_mount.conf.xml**:Fitxer on afegim l'opció de que qualsevol user se li creara un volum temporal de 100MB al inciar sessió.
```
		<!-- Volume definitions -->
<volume
	uid="*"
	fstype="davfs"
	http="https://cloud.proxy.inf.edt.cat:5511/remote.php/dav/files/%(USER)"
	mountpoint="~/cloud"
	options="username=%(USER),uid=%(USERUID),gid=%(USERGID),file_mode=0770,dir_mode=0770"
	/>

		<!-- pam_mount parameters: General tunables -->
```

### Edició startup.sh
Afegir abans del /bin/bash:
```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

/usr/sbin/nscd
/usr/sbin/nslcd

sleep infinity
/bin/bash
```

### Inicialització
```
docker build -t aleixridameya/ldap23:ldap .

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d aleixridameya/ldap23:latest
docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx --privileged -it aleixridameya/pam23:ldap 
```
