# TÚNEL SSH

### Introducció
Per realitzar el túnel:
    - Container "docker" amb servidor phpldapadmin.
    - Container de aws(debian) amb un docker utilitzat com a servidor ldap.

#### AWS:
Inbound rules 22

#### phpldapadmin:
Hem de tenir la clau del EC2 del AWS a dintre del servidor phpldapadmin.

##### Ordres
Primer provarem la connexió amb ssh.
```
ssh -i keypair-2hisx.pem admin@(ip-pública)
```
Editem el **/etc/hosts** del aws i del phpldapadmin.
En el phpldapadmin posem:
```
127.0.0.1   ldap.edt.org
```
En el aws posem:
```
(ip del docker ldap)    ldap.edt.org
```
Relitzem nmap's per comprovar si hem configurat correctament els noms del /etc/hosts.
```
nmap ldap.edt.org
```

#### Túnel
```
ssh -i keypair-2hisx -L 389:ldap.edt.org:389 admin@44.202.134.172
```

#### Comprovació 
NMAP: per comprovar si els ports utilitzats estan oberts
Anem al navegador i provem el phpldapadmin. "(ip_container_php/phpldapadmin)"
