#!/bin/bash
# @aleixridameya ASIX-M06
# ---------------------------


# Share public

mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# Share privat
mkdir -p /var/lib/samba/privat
cp /etc/os-release /var/lib/samba/privat/.

# Configuració samba
cp /opt/docker/smb.conf /etc/samba/smb.conf


# Activar els serveis
/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd -F && echo "nmb  Ok"
