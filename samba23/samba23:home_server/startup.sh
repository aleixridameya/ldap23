#! /bin/bash

# SAMBA 
# Share public

mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# Share privat
mkdir -p /var/lib/samba/privat
cp /etc/os-release /var/lib/samba/privat/.

# Configuració samba
cp /opt/docker/smb.conf /etc/samba/smb.conf


# Activar els serveis
/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd && echo "nmb  Ok"


#PAM
for user in samba01 samba02 samba03 samba04 samba05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

/usr/sbin/nscd
/usr/sbin/nslcd

chmod +x /opt/docker/ldapusers.sh
bash /opt/docker/ldapusers.sh

sleep infinity
#/bin/bash
